import 'package:flutter/material.dart';
import 'package:earental/activity/login.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  @override
  void initState() {
    super.initState();
    new Future.delayed(
        const Duration(seconds: 3),
        () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginPage()),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xFF3096b4), Color(0xFF1e2252)],
          ),
        ),
        child: Center(
          child: new Image.asset(
            'assets/img/eamart_logo.png',
            fit: BoxFit.cover,
            repeat: ImageRepeat.noRepeat,
            width: 240.0,
          ),
        ),
      ),
    );
  }
}
