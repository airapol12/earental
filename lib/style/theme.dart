import 'package:flutter/cupertino.dart';

class Colors {
  const Colors();

  static const Color loginGradientStart = const Color(0xFF3096b4);
  static const Color loginGradientEnd = const Color(0xFF1e2252);
  static const Color loginGradientOrange = const Color(0xFFe27513);
  static const Color loginGradientGray = const Color(0xFF4d4d4d);

  static const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
